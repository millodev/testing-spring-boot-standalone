package com.millodev.standalone;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static java.lang.System.out;

import java.util.Arrays;

@SpringBootApplication
public class StandaloneApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(
			StandaloneApplication.class);
		app.setWebApplicationType(WebApplicationType.NONE);
		app.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		Arrays.stream(args).forEach(s -> out.println(s));
		out.println("Done!");
	}

}